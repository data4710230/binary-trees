/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.tree;

/**
 *
 * @author informatics
 */
public class Node {
     public int iData; // data item (key)
    public double dData; // data item
    public Node leftChild; // this node’s left child
    public Node rightChild; // this node’s right child

    public Node(int iData, double dData) {
        this.iData = iData;
        this.dData = dData;
        this.leftChild = null;
        this.rightChild = null;
    }

    public void displayNode() { // display ourself
        System.out.print("{");
        System.out.print(iData);
        System.out.print(",");
        System.out.print(dData);
        System.out.print("}");
    }
}
